$(function() {
  var ratingArray = ["Zeer slecht", "Slecht", "Voldoende", "Goed", "Zeer goed"];
  var starErrorMessage = "Selecteer hiernaast het betreffend aantal sterren.",
      emailErrorMessage = "Vul een geldig e-mailadres in.",
      feedbackErrorMessage = "Geef hier een onderbouwing omtrent uw feedback.";
  var starImage = '.star--image',
      fullStarClass = 'star--image-fill',
      starTextContainer = '.changeable-text',
      submitButton = '#submit',
      emailContainer = '.email--container';
  var shopsSubmit = '#shopSubmit',
      productSubmit = '#productSubmit',
      researchSubmit = '#researchSubmit';

  function starMouseClick(element) {
    var target = element.attr('for');
    var value = $('#'+target).val();

    element.siblings('.bewertung').val(value);

    if($(emailContainer).length > 0) {
      $(emailContainer).show();
    }
  }

  function starMouseHover(element){
    var target = element.attr('for');
    var value = $('#'+target).val();

    element.parent().siblings('.star--container--text').children(starTextContainer).text(value);
    element.prevAll(starImage).addClass(fullStarClass);
    element.addClass(fullStarClass);
    element.nextAll(starImage).removeClass(fullStarClass);
  }

  function starMouseOut(element){
    var selectedRating = element.siblings('.bewertung').val();
    var selectedIndex = parseInt(selectedRating)-1;

    if(selectedRating > 0){
      // already set rating star
      starMouseHover(element.parent().children(starImage+':eq('+selectedIndex+')'));
    }else{
      element.parent().siblings('.star--container--text').children(starTextContainer).text(selectedRating);
      element.removeClass(fullStarClass);
      element.siblings(starImage).removeClass(fullStarClass);
    }
  }

  function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

  function shopSubmitAction(e) {
    var starIsOkay = false,
        emailIsOkay = false,
        feedbackTextIsOkay = false;

    if(parseInt($("#bewertung").val()) > 0) {
			starIsOkay = true;
		} else {
			alert(starErrorMessage);
			return false;
		}

    if($("#meinung").val().length > 5){
  		feedbackTextIsOkay = true;
  	} else {
  		alert(feedbackErrorMessage);
  		return false;
  	}

    if($('#email').length > 0 || $('#email_check').length > 0){
			if($('#email').val() !== '' || $('#email_check').val() !== ''){
				if(($('#email').val() === $('#email_check').val()) && (validateEmail($('#email').val())) && (validateEmail($('#email_check').val()))) {
          emailIsOkay = true;
        }
			} else {
        emailIsOkay = true;
      }
		}

		if(emailIsOkay === false){
			alert(emailErrorMessage);
			return false;
		}

		if(starIsOkay === true && emailIsOkay === true && feedbackTextIsOkay === true) {
			$('form').submit();
		} else {
			e.preventDefault();
		}
  }

  function productSubmitAction(e) {
    var formElement = $('form');

    var validStars = true;
    var validText = true;

    formElement.find('.bewertung').each(function(index, el) {
      if($(this).val() === '' || $(this).val() < 1){
        validStars = false;
        return false;
      }
    });

    formElement.find('.textarea').each(function(index, el) {
      if($(this).val() === '' || $(this).val().length < 4){
        validText = false;
        return false;
      }
    });

    if(validStars === false){
      alert(starErrorMessage);
      e.preventDefault();
      return false;
    }

    if(validText === false){
      alert(feedbackErrorMessage);
      e.preventDefault();
      return false;
    }

    if(validStars === true && validText === true){
      $('form').submit();
    }else {
      e.preventDefault();
    }
  }

  $(starImage).click(function(){
    starMouseClick($(this));
  });

  $(starImage).hover(function() {
    starMouseHover($(this));
  }, function() {
    starMouseOut($(this));
  });

  $(shopsSubmit).click(function(e) {
    shopSubmitAction(e);
  });

  $(productSubmit).click(function(e) {
    productSubmitAction(e);
  });
});
